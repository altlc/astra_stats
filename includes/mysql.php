<?php

class db_query {
    private $link;
    public $res;
    private $host;
    private $user;
    private $password;
    private $db;

    function result($query)
    {
        if(!$this->link || !mysql_ping($this->link))
        {
            $this->connect();
        }

        //       echo ($query);
        $this->res = mysql_query($query, $this->link);
        return $this->res;
    }

    function data_seek($row)
    {
        return mysql_data_seek($this->res, $row);
    }

    function assoc_array($query)
    {
        if ($this->res = $this->result($query))
        {
              $ret = mysql_fetch_assoc($this->res);
        }
        return $ret;
    }

    function fetch_assoc()
    {
        return mysql_fetch_assoc($this->res);
    }

    function affected_rows()
    {
        return mysql_affected_rows($this->link);
    }

    function error()
    {
        return mysql_error($this->link);
    }

    function insert_id()
    {
        return mysql_insert_id($this->link);
    }

    function client_encoding()
    {
        return mysql_client_encoding($this->link);
    }

    private function connect()
    {
          $this->link = mysql_connect($this->host,$this->user,$this->password,true);
          if (!$this->link)
          {
            echo "Ошибка соединится с сервером!!!";
            exit;
          }

          if (!mysql_select_db($this->db, $this->link) )
          {
            echo "Ошибка выбора базы!!!";
            exit;
          }

          @mysql_query("SET NAMES 'utf8'", $this->link);
          @mysql_query("SET CHARACTER SET utf8", $this->link);
    }

    function __construct($host = SQL_HOST, $user = SQL_USER, $password = SQL_PASSWORD, $db = SQL_DB) {
          if (!$this->link)
          {
              $this->host = $host;
              $this->user = $user;
              $this->password = $password;
              $this->db = $db;  
              $this->connect();
         }
    }

    function __destruct() {
          //if ($this->link)
          //{
          //      mysql_close($this->link);
          //}

          if ($this->res)
          {
                @mysql_free_result($this->res);
          }
    }

};

?>