<?php
  if ( defined('XMPPLOGIN')) {
  
        $query = new db_query();
        $lock_query = new db_query();
        
        $lock_query->result("lock tables `lock` write");
        
        $query->result("select log.*, send_xmpp from log left join channels using(server,output)
                               where xmpp_is_sent = 0 and send_xmpp = 1
                               order by time limit 20");
        
        $update = Array();
          
        if ($query->affected_rows() > 0) {
            $conn = new XMPPHP_XMPP(XMPPHOST, XMPPPORT, XMPPLOGIN, XMPPPASS, 'xmpphp', XMPPDOMAIN, $printlog=false, $loglevel=XMPPHP_Log::LEVEL_INFO);
            try {
                  $conn->connect();
                  $conn->processUntil('session_start');
                  $conn->presence();
            }catch(XMPPHP_Exception $e) {
                $query->result("UNLOCK TABLES");  
                die($e->getMessage());
            }
        }

        while (is_array($log = $query->fetch_assoc()))
        {
	    $log['ready'] = $log['ready']?'Да - *OK*':'Нет - *HELP*';
            $log['scrambled'] = $log['scrambled']?'Да - ]:->':'Нет - 8-)';
            $log['cam'] = $log['cam']?'Да':'Нет';
            $log['keys'] = $log['keys']?'Да':'Нет';

            $message = "Время: ".$log['time'].
                       "\nКанал: ".$log['channel'].
                       "\nСервер: ".$log['server'].
                       "\nРаботает: ".$log['ready'].
                       "\nЗашифрован: ".$log['scrambled'].
                       "\nCAM: ".$log['cam'].
                       "\nКлючи: ".$log['keys'];
            
            try {
                  $conn->message(XMPPALERTJID, $message);
                  $update[] = $log['log_id'];
            }catch(XMPPHP_Exception $e) {
                die($e->getMessage());
            }
        }
        
        if (is_array($update)){
            foreach ($update as $log_id) {
                $query->result("update log set xmpp_is_sent = 1 where log_id=".$log_id);
                echo $query->error();
            }
        }
                
        if (isset($conn) && is_object($conn)) $conn->disconnect();
  }else{
      $query = new db_query(); 
      $query->result("update log set xmpp_is_sent = 1");
  }
  

?>
