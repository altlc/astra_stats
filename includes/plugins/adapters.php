<?php
  function plugin_adapters_main() {
          global $data, $lang;
          $query = new db_query();
                  
          $query->result("select * from adapters order by server,adapter");
          
          $num = 0;
          
          $adapters = Array();
          $adapters['list'] = '';
          
          while (is_array($adapter = $query->fetch_assoc()))
          {
              $num++;  
              $adapter['num'] = $num;
              
              if ($num%2) {
                  $adapter['bgcolor'] = '#FCFCFC';
              }else{
                  $adapter['bgcolor'] = '';
              }
              
              if ($adapter['lock'])
              {
                  $adapter['lock_title'] = $lang[LANG]['Signal OK'];
                  $adapter['lock_image'] = 'antenna.png';
                  $adapter['lock_bgcolor'] = $adapter['bgcolor'];
              }else{
                  $adapter['lock_title'] = $lang['en']['No Signal'];
                  $adapter['lock_image'] = 'antenna_red.png';
                  $adapter['lock_bgcolor'] = 'yellow';
              }
              
              if ($adapter['bitrate'] < 100 ){
                  $adapter['bitrate_bgcolor'] = '#FDD';
              }else{
                  $adapter['bitrate_bgcolor'] = $adapter['bgcolor'];
              }
              
              $adapter['bitrate'] = intval($adapter['bitrate'])." kBit";
              $adapter['stream_encoded'] = rawurlencode( $adapter['stream']);
              
              $adapters['list'] .= template_parse('adapters/adapter.html',$adapter);
              
          }
          
          $data['page'] = template_parse('adapters/adapters_list.html',$adapters);
           $data['page'] .= "<script type='text/javascript'>
            window.setTimeout('location.reload()', ".(PAGE_RELOAD_TIME*1000).");
          </script>";
  }
  
  function plugin_adapters_delete_adapter() {
          global $data;
          
          $query = new db_query();
          if (intval($data['adapter_id'])) $query->result("delete from adapters where adapter_id=".intval($data['adapter_id']));
          header('Location: '.$_SERVER['PHP_SELF'].'?plugin=adapters',true, 303);
          exit;
  }
?>
