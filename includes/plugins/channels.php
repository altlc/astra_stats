<?php

  function plugin_channels_main() {
          global $data, $lang;
          
          if (!isset($data['order_by'])) $data['order_by'] = 'channel';
          
          if (!isset($data['order']) || $data['order'] == 'desc') 
          {
            $data['order'] = 'asc';
          }else{
            $data['order'] = 'desc';  
          }
          
          $data['filter'] = '';
                    
          if(isset($data['stream']) && $data['stream']!='') 
          {
              $where = (isset($where)?$where.' and ':'').'channels.stream="'.mysql_escape_string($data['stream']).'"';
              $data['filter'].="&stream=".rawurlencode($data['stream']);
          }else{
              $data['stream']='';
          }
          
          if(isset($data['server']) && $data['server']!='') 
          {
              $where = (isset($where)?$where.' and ':'').'channels.server="'.mysql_escape_string($data['server']).'"';
              $data['filter'].="&server=".rawurlencode($data['server']);
          }else{
              $data['server']='';
          }
          

          $query = new db_query();
          $query->result("select channels.* , `keys`, `cam`, decrypt_id, TIME_TO_SEC(timediff(now(),last_update)) as last_update_period
                                 from channels left join decrypt using(server,output)".(isset($where)?' where '.$where:''));
          
          $num = 0;
          
          $channels = Array();
          $channels['list'] = '';
          
          while (is_array($channel = $query->fetch_assoc()))
          {
              $tmp_channels[] = $channel;
              $order_channels[] = $channel['channel'];
              //echo $data['order_by'];
              if ($data['order_by'] != 'output')
              {
                  $sort_order[] = $channel[$data['order_by']];
              }else{
                  $url_parts = parse_url($channel['output']);  
                  if(ip2long($url_parts["host"])) {
                      $sort_order[] = ip_2_long($url_parts["host"]);
                  } else {
                      $sort_order[] = $url_parts["host"];
                  }
              }
          }
          
          //var_dump($sort_order);
          if (is_array($tmp_channels))
          {
              array_multisort($sort_order, ( $data['order'] == 'asc'?SORT_ASC:SORT_DESC ), $order_channels, SORT_ASC, $tmp_channels);
              foreach($tmp_channels as $channel )
              {
                  $num++;  
                  $channel['num'] = $num;
                  
                  //$channel['channel'] = str_replace("udp://","udp://@" ,$channel['channel']);
                  if ($num%2) {
                      $channel['bgcolor'] = '#FCFCFC';
                  }else{
                      $channel['bgcolor'] = '';
                  }
                  
                  if ($channel['bitrate'] < 100 ){
                      $channel['bitrate_bgcolor'] = '#FDD';
                  }else{
                      $channel['bitrate_bgcolor'] = $channel['bgcolor'];
                  }
                  $channel['bitrate'] = intval($channel['bitrate'])." kBit";

                  
                  if ($channel['ready'])
                  {
                      $channel['ready_title'] = $lang[LANG]['Connected'];
                      $channel['ready_image'] = 'connect.png';
                      $channel['ready_bgcolor'] = $channel['bgcolor'];
                  }else{
                      $channel['ready_image'] = 'disconnect.png';
                      $channel['ready_bgcolor'] = 'yellow';
                      
                      if ($channel['scrambled']) {
                          $channel['ready_title'] = $lang[LANG]['No channel on the satellite, or a wrong channel number'];
                      }else{
                          $channel['ready_title'] = $lang[LANG]['It is Not Working'];
                      }
                      
                  }
                  
                  
                  if ($channel['scrambled'])
                  {
                      $channel['scrambled_title']='';
                      if ($channel['ready'])
                      {
                          $channel['scrambled_title'] .= $lang[LANG]['Encrypted'];
                          if (!$channel['cam']) {
                                $channel['scrambled_title'] .= $lang[LANG]['Cannot Connect to CAM Server'];
                          }else{
                              if(!$channel['keys']) {
                                   $channel['scrambled_title'] .= $lang[LANG]['No Subscription'];
                              }
                          }
                      } 
                      
                      $channel['scrambled_image'] = 'lock.png';
                      $channel['scrambled_bgcolor'] = 'orange';
                  }else{
                      $channel['scrambled_title'] = $lang[LANG]['Unscrambled'];
                      $channel['scrambled_image'] = 'lock_unlock.png';
                      $channel['scrambled_bgcolor'] = $channel['bgcolor'];
                      if ($channel['cam'] && !$channel['keys']) 
                      {
                         $channel['scrambled_title'] .= $lang[LANG]['The channel is free. Cam on'];
                         $channel['scrambled_bgcolor'] = '#D5FFD5';
                         $channel['scrambled_image'] = 'monitor.png'; 
                      }elseif(!$channel['cam'] && !$channel['keys']){
                         $channel['scrambled_title'] .= $lang[LANG]['The channel is free. Cam off'];
                         $channel['scrambled_image'] = 'monitor.png'; 
                      }
                  }
                  
                  if ($channel['last_update_period'] > 30*60 )
                  {
                      $channel['last_update_bgcolor'] = 'red';
                  }else{
                      $channel['last_update_bgcolor'] = $channel['bgcolor'];
                  }
                  
                  
                  if ($channel['cc_error'] > 5 )
                  {
                      $channel['cc_error_bgcolor'] = '#C4DEFF';
                  }else{
                      $channel['cc_error_bgcolor'] = $channel['bgcolor'];
                  }
                  
                  
                  if ($channel['pes_error'] > 5 )
                  {
                      $channel['pes_error_bgcolor'] = '#C4DEFF';
                  }else{
                      $channel['pes_error_bgcolor'] = $channel['bgcolor'];
                  }
                  
                  $url_parts = parse_url($channel['output']);
                  $channel['output'] =  $url_parts["scheme"].'://@'.$url_parts["host"].(isset($url_parts["port"])?':'.$url_parts["port"]:'');  
                  
                  htmlspecialchars_array($channel);
                  
                  $channel['send_sms_checked'] = $channel['send_sms']?'checked="checked"':'';
                  $channel['send_xmpp_checked'] = $channel['send_xmpp']?'checked="checked"':'';
                  $channel['send_email_checked'] = $channel['send_email']?'checked="checked"':'';
                  
                  $channel['stream_encoded'] = rawurlencode($channel['stream']);
                  $channel['filter'] = $data['filter'];
                  $channels['list'] .= template_parse('channels/channel.html',$channel);
              }
          }
          
          $channels['order'] = $data['order'];
          $channels['order_by'] = $data['order_by'];
          $channels['filter'] = $data['filter'];
          $channels['filter_server'] = servers_select($data['server']);
          $channels['filter_stream'] = streams_select($data['stream']);
          
          $data['page'] = template_parse('channels/channels_list.html',$channels);
          
          $data['page'] .= "<script type='text/javascript'>
            window.setTimeout('location.reload()', ".(PAGE_RELOAD_TIME*1000).");
          </script>";
  }
  
  function plugin_channels_delete_channel() {
          global $data;
          
          $query = new db_query();
          if (intval($data['channel_id'])) $query->result("delete from channels where channel_id=".intval($data['channel_id']));
          if (intval($data['decrypt_id'])) $query->result("delete from decrypt where decrypt_id=".intval($data['decrypt_id']));
          header('Location: '.$_SERVER['PHP_SELF'].'?plugin=channels',true, 303);
          exit;
  }
  
  function plugin_channels_set_sms() {
          global $data;
          
          $query = new db_query();
          //var_dump($data);
          $data['send_sms'] = isset($data['send_sms'])?1:0;
          
          $query->result("update channels set send_sms=".$data['send_sms']." where channel_id=".intval($data['channel_id']));
          header('Location: '.$_SERVER['PHP_SELF '].'?plugin=channels',true, 303);
          exit;
  }
  
  

    
  function plugin_channels_set_xmpp() {
          global $data;
          
          $query = new db_query();
          //var_dump($data);
          $data['send_xmpp'] = isset($data['send_xmpp'])?1:0;
          
          $query->result("update channels set send_xmpp=".$data['send_xmpp']." where channel_id=".intval($data['channel_id']));
          header('Location: '.$_SERVER['PHP_SELF '].'?plugin=channels',true, 303);
          exit;
  }
  
  function plugin_channels_set_email() {
          global $data;
          
          $query = new db_query();
          //var_dump($data);
          $data['send_email'] = isset($data['send_email'])?1:0;
          
          $query->result("update channels set send_email=".$data['send_email']." where channel_id=".intval($data['channel_id']));
          header('Location: '.$_SERVER['PHP_SELF '].'?plugin=channels',true, 303);
          exit;
  }
  
  function plugin_channels_play_channel() {
          global $data;
          
          $query = new db_query();
          $channel = $query->assoc_array("select * from channels where channel_id=".intval($data['channel_id']));

          $url_parts = parse_url($channel['output']);
          
          $channel['mrl'] =  $url_parts["scheme"].'://@'.$url_parts["host"].(isset($url_parts["port"])?':'.$url_parts["port"]:'');  
          
          if(defined('UDPXY_HOST') || defined('UDPXY_HOSTONSERVER') )
          {
              //http://x.x.x.x:4050/udp/224.0.90.183:1234
              $host = defined('UDPXY_HOSTONSERVER')?$channel["server"]:UDPXY_HOST;
              $channel['output'] =  'http://'.$host.':'.UDPXY_PORT.'/'.$url_parts["scheme"].'/'.$url_parts["host"].(isset($url_parts["port"])?':'.$url_parts["port"]:'');
          }else{
              $channel['output'] =  $channel['mrl'];
          }

          echo template_parse("channels/player.html", $channel);
          exit;
  }
  
  
?>
