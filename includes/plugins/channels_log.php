<?php

  function plugin_channels_log_main() {
          global $data, $lang;
          
          $query = new db_query();
          $filter='';
          
          if (isset($data['channel_id']))
          {
              $where = "channel_id = ".intval($data['channel_id']);
              $chan_mame = $query->assoc_array("select * from channels where channel_id=".intval($data['channel_id']));
              $filter.=$lang[LANG]['by_channel']."<strong>".$chan_mame['channel']."</strong>";
          }
          
          if (isset($data['server']) && $data['server']!='')
          {
              $where = (isset($where)?$where.' and ':'')." channels.server = '".mysql_escape_string($data['server'])."'";
              $filter.=$lang[LANG]['by_server']." <strong>".$data['server']."</strong>";
          }
          $filter=$filter?$lang[LANG]['Filter by'].$filter."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='".$_SERVER['PHP_SELF']."?plugin=channels_log' >".$lang[LANG]['Remove']."</a>":'';
          
          $query->result("select log.*, channels.channel as channel, channel_id
                                 from log left join channels using(server,output)
                                 ".(isset($where)?' where '.$where:'')."
                                 order by time desc limit ".LOG_LINES);
          
          $num = 0;
          
          $log = Array();
          $log['list'] = '';
          
          while (is_array($log_line = $query->fetch_assoc()))
          {
              $num++;  
              $log_line['num'] = $num;
              
              //$log_line['channel'] = str_replace("udp://","udp://@" ,$log_line['channel']);
              if ($num%2) {
                  $log_line['bgcolor'] = '#FCFCFC';
              }else{
                  $log_line['bgcolor'] = '';
              }
              
              if ($log_line['ready'])
              {
                  $log_line['ready_title'] = $lang[LANG]['Connected'];
                  $log_line['ready_image'] = 'connect.png';
                  $log_line['ready_bgcolor'] = $log_line['bgcolor'];
              }else{
                  $log_line['ready_image'] = 'disconnect.png';
                  $log_line['ready_bgcolor'] = 'yellow';
                  
                  if ($log_line['scrambled']) $log_line['ready_title'] = $lang[LANG]['No channel on the satellite, or a wrong channel number'];
                  
              }
              
              
              if ($log_line['scrambled'])
              {
                  $log_line['scrambled_title']='';
                  if ($log_line['ready'])
                  {
                      $log_line['scrambled_title'] .= $lang[LANG]['Encrypted'];;
                      if (!$log_line['cam']) {
                            $log_line['scrambled_title'] .= $lang[LANG]['Cannot Connect to CAM Server'];
                      }else{
                          if(!$log_line['keys']) {
                               $log_line['scrambled_title'] .= $lang[LANG]['No Subscription'];
                          }
                      }
                  } 
                  
                  $log_line['scrambled_image'] = 'lock.png';
                  $log_line['scrambled_bgcolor'] = 'orange';
              }else{
                  $log_line['scrambled_title'] = $lang[LANG]['Unscrambled'];
                  $log_line['scrambled_image'] = 'lock_unlock.png';
                  $log_line['scrambled_bgcolor'] = $log_line['bgcolor'];
              }
              
              $log_line['cam_image'] = $log_line['cam']?'server.png':'server_error.png';
              $log_line['cam_title'] = $log_line['cam']?$lang[LANG]['Key Server Connected']:$lang[LANG]['Key Server Disabled'];
              
              $log_line['keys_image'] = $log_line['keys']?'key_green.png':'key_red.png';
              $log_line['keys_title'] = $log_line['keys']?$lang[LANG]['Keys Present']:$lang[LANG]['No Keys'];
              
              htmlspecialchars_array($log_line);
              
              $log['list'] .= template_parse('channels_log/log_line.html',$log_line);
          }
          
          $log['filter']=$filter;
          $data['page'] = template_parse('channels_log/channels_log_list.html',$log);
           $data['page'] .= "<script type='text/javascript'>
            window.setTimeout('location.reload()', ".(PAGE_RELOAD_TIME*1000).");
          </script>";
  }

  
  function plugin_channels_log_clean_log() {
      $query = new db_query();
      $query ->result("delete from log");
      header('Location: '.$_SERVER['PHP_SELF'].'?plugin=channels_log',true, 303);
      exit;
  }
?>
