<?php
  function plugin_playlist_main() {
          global $data;
          
          $data['page'] = template_parse("playlist/main_form.html", $data);
          
  }
  
  function plugin_playlist_get_m3u() {
          global $data;
          
          $query = new db_query();
          
          $where = Array();
                    
          if(!isset($data['get_scrambled']))
          {
                $where[] = 'scrambled=0';
          }
          
          if(!isset($data['get_not_working']))
          {
                $where[] = 'ready=1';
          }

          if (count($where))
          {
              $where_str = ' where '; 
              foreach ($where as $key => $val)
              {
                    $where_str .= $key?' and '.$val.' ':' '.$val.' ';
              }
          }
          
          header("Content-type: audio/mpegurl");
          header("Content-Disposition: attachment; filename=playlist.m3u" );
          header("Expires: 0");
          header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
          header("Pragma: public");
          
          $query->result("select * from channels ".$where_str." order by channel");
          while (is_array($channel = $query->fetch_assoc() ))
          {
              $url_parts = parse_url($channel['output']);
              
              if(defined('UDPXY_HOST') && $data['use_udpxy'])
              {
                 //http://x.x.x.x:4050/udp/224.0.90.183:1234
                 $channel['output'] =  'http://'.UDPXY_HOST.':'.UDPXY_PORT.'/'.$url_parts["scheme"].'/'.$url_parts["host"].(isset($url_parts["port"])?':'.$url_parts["port"]:'');
              }else{
                 $channel['output'] =  $url_parts["scheme"].'://@'.$url_parts["host"].(isset($url_parts["port"])?':'.$url_parts["port"]:'');
              }


              echo '#EXTINF:0,'.$channel['channel'].''."\n";
//              echo '#EXTVLCOPT:deinterlace=-1'."\n";
//              echo '#EXTVLCOPT:deinterlace-mode='.($channel['bitrate']<5120000?'yadif2x':'yadif')."\n";
//              echo '#EXTVLCOPT:udp-caching=2000'."\n";
              echo $channel['output']."\n\n";
              
          }
          
          exit;
          
  }

  
?>


