<?php
    function servers_select($selected_server)
    {
        global $lang;
        $query = new db_query();
        $query->result('select DISTINCT server from channels order by server');
        
        $return = "<select name='server' id='server'>\n";
        $return .= "<option ".($selected_server==''?'selected':'')." value=''>".$lang[LANG]['Any']."</option>\n";
        while (is_array($server = $query->fetch_assoc()))
        {
            $return .= "<option ".($selected_server == $server['server']?'selected':'')." value='".$server['server']."'>".$server['server']."</option>\n";
        }
        $return .= "</select>\n";
        return $return;
    }
    
    function streams_select($selected_stream)
    {
        global $lang;
        $query = new db_query();
        $query->result('select DISTINCT stream from channels order by stream');
        
        $return = "<select name='stream' id='stream'>\n";
        $return .= "<option ".($selected_stream==''?'selected':'')." value=''>".$lang[LANG]['Any']."</option>\n";
        while (is_array($stream = $query->fetch_assoc()))
        {
            $return .= "<option ".($selected_stream == $stream['stream']?'selected':'')." value='".$stream['stream']."'>".$stream['stream']."</option>\n";
        }
        $return .= "</select>\n";
        return $return;
    }
?>
