<?php
//Подготовка массива к выводу.
//-----------------------------------------
function htmlspecialchars_array(&$data)
{
     $data=array_map('htmlspecialchars_array_callback',$data);
}

function htmlspecialchars_array_callback($val)
{
	return htmlspecialchars(trim($val),ENT_QUOTES);
}


function ip_2_long($ip)
{
    return sprintf("%u",ip2long($ip));
}

function is_ip($ip) {
  $valid = TRUE;
  $ip = explode(".", $ip);
  if(count($ip)!=4) {
      return FALSE;
      }
  foreach($ip as $key => $block) {
      if(!is_numeric($block) || $block>=255 || ((!$key || $key==3) && $block<1) ) {
          $valid = FALSE;
      }
  }
  return $valid;
}


function is_ip_mask($ip) {
  $valid = TRUE;

  $ip = explode(".", $ip);
  if(count($ip)!=4) {
      return FALSE;
      }
  foreach($ip as $key => $block) {
      if(!is_numeric($block) || $block > 255 || ((!$key || $key == 3) && $block < 0) ) {
          $valid = FALSE;
      }
  }
  return $valid;
}

?>