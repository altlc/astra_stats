DROP TABLE IF EXISTS `adapters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adapters` (
  `adapter_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `adapter` int(11) unsigned NOT NULL DEFAULT '0',
  `server` varchar(100) NOT NULL DEFAULT '',
  `stream` varchar(100) NOT NULL DEFAULT '',
  `lock` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `snr` int(11) unsigned NOT NULL DEFAULT '0',
  `bitrate` int(11) unsigned NOT NULL DEFAULT '0',
  `unc` int(11) unsigned NOT NULL DEFAULT '0',
  `ber` int(11) unsigned NOT NULL DEFAULT '0',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`adapter_id`),
  UNIQUE KEY `adapter_server` (`adapter`,`server`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channels` (
  `channel_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(100) NOT NULL DEFAULT '' COMMENT 'имя хоста',
  `output` varchar(100) NOT NULL DEFAULT '' COMMENT 'канал, используется первый элемент списка output',
  `channel` varchar(100) NOT NULL DEFAULT '' COMMENT 'название канала',
  `stream` varchar(100) NOT NULL DEFAULT '' COMMENT 'названия потока',
  `ready` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'устанавливается в true если получена вся необходимая информация из потока',
  `pnr` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Номер канала на трансподере',
  `cc_error` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'счётчик CC-ошибок',
  `pes_error` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'счётчик ошибок PES',
  `bitrate` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'скорость потока',
  `scrambled` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'зашифрован или нет канал',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'время последнего обновления',
  `send_sms` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'Отправка sms при изменении статуса',
  `send_xmpp` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'Отправка xmpp при изменении статуса',
  `send_email` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'Отправка email при изменении статуса',
  PRIMARY KEY (`channel_id`),
  UNIQUE KEY `output_server` (`server`,`output`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 */ /*!50003 TRIGGER `channel_log_trigger` BEFORE UPDATE ON `channels`
  FOR EACH ROW begin
    if new.ready != old.ready or new.scrambled != old.scrambled then
      insert delayed into `log` set server = new.server,
             `channel` = new.channel,             
             `output` = new.output,             
             `ready` = new.ready,
             `scrambled` = new.scrambled,
             `cam` = (select `cam` from decrypt where server = new.server and output = new.output),
             `keys` = (select `keys` from decrypt where server = new.server and output = new.output);
    end if;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
DROP TABLE IF EXISTS `decrypt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `decrypt` (
  `decrypt_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(100) NOT NULL DEFAULT '' COMMENT 'имя хоста',
  `output` varchar(100) NOT NULL DEFAULT '' COMMENT 'канал, используется первый элемент списка output',
  `channel` varchar(100) NOT NULL DEFAULT '',
  `stream` varchar(100) NOT NULL DEFAULT '',
  `keys` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'ошибка обработки ECM пакета (нет подписки)',
  `cam` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'ошибка соединения с CAM-сервером',
  PRIMARY KEY (`decrypt_id`),
  UNIQUE KEY `cr_server_output` (`server`,`output`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `lock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `log_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `server` varchar(200) NOT NULL DEFAULT '' COMMENT 'имя хоста',
  `output` varchar(100) NOT NULL DEFAULT '' COMMENT 'канал, используется первый элемент списка output',
  `channel` varchar(100) NOT NULL DEFAULT '',
  `ready` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `scrambled` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `keys` tinyint(3) unsigned DEFAULT '0' COMMENT 'ошибка обработки ECM пакета (нет подписки)',
  `cam` tinyint(3) unsigned DEFAULT '0' COMMENT 'ошибка соединения с CAM-сервером',
  `xmpp_is_sent` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `sms_is_sent` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mail_is_sent` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`log_id`),
  KEY `alerts_sented` (`xmpp_is_sent`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_password` (`user`,`password`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
