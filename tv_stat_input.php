<?php
require_once("includes/config.php");
require_once("includes/mysql.php");

$json_request = json_decode(file_get_contents('php://input'), true);

$query = new db_query();

switch ($json_request['type']) {
	case "channel":

//	$channel_ip = str_replace('udp://','',$json_request['channel']);
	    $ret = $query->result("insert into channels set
                               server = '".mysql_escape_string($json_request['server'])."',
                               output = '".mysql_escape_string($json_request['output'])."',
                               channel = '".mysql_escape_string($json_request['channel'])."',
                               stream = '".mysql_escape_string($json_request['stream'])."',
                               pnr = '".mysql_escape_string($json_request['pnr'])."',
                               scrambled = '".mysql_escape_string($json_request['scrambled'])."',
						       cc_error = '".mysql_escape_string($json_request['cc_error'])."',
						       pes_error = '".mysql_escape_string($json_request['pes_error'])."',
						       bitrate = '".mysql_escape_string($json_request['bitrate'])."',
						       ready = '".mysql_escape_string($json_request['ready'])."',
                               last_update = now()
						       ON DUPLICATE KEY UPDATE
                               channel = '".mysql_escape_string($json_request['channel'])."',
                               stream = '".mysql_escape_string($json_request['stream'])."',
                               pnr = '".mysql_escape_string($json_request['pnr'])."',
						       scrambled = '".mysql_escape_string($json_request['scrambled'])."',
                               cc_error = '".mysql_escape_string($json_request['cc_error'])."',
						       pes_error = '".mysql_escape_string($json_request['pes_error'])."',
						       bitrate = '".mysql_escape_string($json_request['bitrate'])."',
						       ready = '".mysql_escape_string($json_request['ready'])."',
						       last_update = now()");
                               
//	    syslog(LOG_WARNING,$ret);
        break;
	case "dvb":
	    $ret = $query->result("insert into adapters set adapter = '".mysql_escape_string($json_request['adapter'])."',
						       server = '".mysql_escape_string($json_request['server'])."',
                               stream = '".mysql_escape_string($json_request['stream'])."',
						       `lock` = '".mysql_escape_string($json_request['lock'])."',
						       bitrate = '".mysql_escape_string($json_request['bitrate'])."',
						       snr = '".mysql_escape_string($json_request['snr'])."',
						       unc = '".mysql_escape_string($json_request['unc'])."',
						       ber = '".mysql_escape_string($json_request['ber'])."'
						       ON DUPLICATE KEY UPDATE
						       stream = '".mysql_escape_string($json_request['stream'])."',
                               `lock` = '".mysql_escape_string($json_request['lock'])."',
						       bitrate = '".mysql_escape_string($json_request['bitrate'])."',
						       snr = '".mysql_escape_string($json_request['snr'])."',
						       unc = '".mysql_escape_string($json_request['unc'])."',
						       ber = '".mysql_escape_string($json_request['ber'])."'
						       	");
	    //syslog(LOG_WARNING,$ret);

	    break;
	
    case 'decrypt':
        $ret = $query->result("insert into decrypt set
                               server = '".mysql_escape_string($json_request['server'])."',
                               output = '".mysql_escape_string($json_request['output'])."',
                               channel = '".mysql_escape_string($json_request['channel'])."',
                               stream = '".mysql_escape_string($json_request['stream'])."',
                               `keys` = '".mysql_escape_string($json_request['keys'])."',
                               cam = '".mysql_escape_string($json_request['cam'])."'
                               ON DUPLICATE KEY UPDATE
                               channel = '".mysql_escape_string($json_request['channel'])."',
                               stream = '".mysql_escape_string($json_request['stream'])."',
                               `keys` = '".mysql_escape_string($json_request['keys'])."',
                               cam = '".mysql_escape_string($json_request['cam'])."'
                               ");
        
        //syslog(LOG_WARNING,$ret);
        break;
	    
	default:
	    break;
}

        //Сообщения через xmpp/jabber
        if (defined('XMPPLOGIN')) {
            require_once("includes/XMPPHP/XMPP.php");
        }
        require_once("includes/xmpp_alerts.php");
        
        //Cvc через devinotele.com
        if (defined('DEVINO_LOGIN')) {
            require_once("includes/devinosms.php");
        }
        require_once("includes/sms_alerts.php");                     


    $log = $query->assoc_array("select count(*) as num from log");
    if($log['num'] > LOG_LINES)
    {
        $query->result("delete from log order by log_id limit 1");
    }


?>

